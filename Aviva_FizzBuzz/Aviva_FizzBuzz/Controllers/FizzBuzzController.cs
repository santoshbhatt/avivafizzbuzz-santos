﻿using Aviva_FizzBuzz.Service.Contract;
using Aviva_FizzBuzz.Service.Service;
using Aviva_FizzBuzz.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Aviva_FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
        private IFizzBuzzService service;

        public FizzBuzzController(IFizzBuzzService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Display user input form
        /// </summary>
        /// <returns> Return FizzBuzz view</returns>
        public ActionResult FizzBuzzUserInput()
        {
            return View(viewName: "FizzBuzz");
        }

        /// <summary>
        /// Output FizzBuzz list
        /// </summary>
        /// <param name="fizzBuzzViewModel"> Action requries viewmodel as input parameter</param>
        /// <param name="page">Parameter page number</param>
        /// <returns> FizzBuzz view with Model data </returns>
        public ActionResult GetFizzBuzzList(FizzBuzzViewModel fizzBuzzViewModel, int? page = 1)
        {
            if (ModelState.IsValid)
            {
                int pageSize = 20;
                int pageNumber = page ?? 1;

                fizzBuzzViewModel.DisplayData = this.service.GetFizzBuzzData(fizzBuzzViewModel.UserInput).ToPagedList(pageNumber, pageSize);
                return this.View("FizzBuzz", fizzBuzzViewModel);
            }
            else
            {
                return this.View("FizzBuzz");
            }

        }
    }
}
