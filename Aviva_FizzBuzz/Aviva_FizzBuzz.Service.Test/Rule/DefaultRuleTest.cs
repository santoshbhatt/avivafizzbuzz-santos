﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class DefaultRuleTest
    {
        /// <summary>
        /// DefaultRule object declaration
        /// </summary>
        private DefaultRule defaultRule;

        /// <summary>
        /// DefaultRule object creation.
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.defaultRule = new DefaultRule();
        }

        /// <summary>
        /// Returns number when not divisible by three and five
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            bool result;

            result = this.defaultRule.ModCheck(1);

            result.Should().Be(true);
        }

        /// <summary>
        /// Returns number
        /// </summary>
        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;

            output = this.defaultRule.Print(1, DayOfWeek.Monday);

            output.Should().Be("1");
        }
    }
}
