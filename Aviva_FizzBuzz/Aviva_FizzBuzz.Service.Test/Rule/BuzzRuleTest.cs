﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class BuzzRuleTest
    {
        /// <summary>
        /// BuzzRule object declaration
        /// </summary>
        private BuzzRule buzzRule;

        /// <summary>
        /// BuzzRule object creation.
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            buzzRule = new BuzzRule();
        }

        /// <summary>
        /// ModCheck returns true when number is divisible by five.
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByFive()
        {
            bool result;

            result = this.buzzRule.ModCheck(5);

            result.Should().Be(true);
        }

        /// <summary>
        /// Returns buzz
        /// </summary>
        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;

            output = this.buzzRule.Print(5, DayOfWeek.Monday);

            output.Should().Be("buzz");
        }

        /// <summary>
        /// Return wuzz when day of week is Wednesday.
        /// </summary>
        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;

            output = this.buzzRule.Print(5, DayOfWeek.Wednesday);

            output.Should().Be("wuzz");
        }
    }
}
