﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using System.Collections.Generic;
using Aviva_FizzBuzz.Service.Rule;

namespace Aviva_FizzBuzz.Service.Test.Rule
{
    [TestFixture]
    public class FizzRuleTest
    {
        /// <summary>
        /// FizzRule object declaration.
        /// </summary>
        private FizzRule fizzRule;

        /// <summary>
        /// FizzRule object creation...
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.fizzRule = new FizzRule();
        }

        /// <summary>
        /// ModCheck method returns true when number is divisible by three.
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByThree()
        {
            bool result;

            result = this.fizzRule.ModCheck(3);

            result.Should().Be(true);
        }

        /// <summary>
        /// Returns fizz
        /// </summary>
        [Test]
        public void Print_ShouldReturnFizz_WhenCalled()
        {
            string output;

            output = this.fizzRule.Print(3, DayOfWeek.Monday);

            output.Should().Be("fizz");
        }

        /// <summary>
        /// Return wizz when day of week is Wednesday.
        /// </summary>
        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;

            output = this.fizzRule.Print(3, DayOfWeek.Wednesday);

            output.Should().Be("wizz");
        }
    }
}
