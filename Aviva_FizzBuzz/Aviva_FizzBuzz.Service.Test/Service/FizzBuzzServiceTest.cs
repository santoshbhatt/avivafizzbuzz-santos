﻿using System;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using System.Collections.Generic;
using Aviva_FizzBuzz.Service.Service;
using Aviva_FizzBuzz.Service.Rule;
using Aviva_FizzBuzz.Service.Contract;

namespace Aviva_FizzBuzz.Service.Test.Service
{
    [TestFixture]
    public class FizzBuzzServiceTest
    {
        private FizzBuzzService service;

        private List<string> fizzBuzzList = null;

        private Mock<FizzRule> fizzRule = new Mock<FizzRule>();

        private Mock<BuzzRule> buzzRule = new Mock<BuzzRule>();

        private Mock<DefaultRule> defaultRule = new Mock<DefaultRule>();

        private List<IRule> rules;

        /// <summary>
        /// Setup method for all tests.
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.rules = new List<IRule>();
            this.rules.Add(this.fizzRule.Object);
            this.rules.Add(this.buzzRule.Object);
            this.rules.Add(this.defaultRule.Object);
            this.service = new FizzBuzzService(this.rules);
        }

        /// <summary>
        /// GetFizzBuzzData method should return list object having count equal to user entered number...
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnListCount_EqualToUserInput()
        {
            fizzBuzzList = this.service.GetFizzBuzzData(15);

            fizzBuzzList.Count.Should().Be(15);
        }

        /// <summary>
        /// GetFizzBuzzData method returns fizz when number is divisible by three.
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnFizz_WhenNumberIsDivisibleByThree()
        {
            this.fizzRule.Setup(m => m.ModCheck(3)).Returns(true);
            this.fizzRule.Setup(m => m.Print(3, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.fizzBuzzList = this.service.GetFizzBuzzData(3);

            this.fizzBuzzList[2].Should().BeOneOf("fizz", "wizz");
        }

        /// <summary>
        /// GetFizzBuzzData method returns buzz when number is divisible by five.
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnBuzz_WhenNumberIsDivisibleByFive()
        {
            this.buzzRule.Setup(m => m.ModCheck(5)).Returns(true);
            this.buzzRule.Setup(m => m.Print(5, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzList = this.service.GetFizzBuzzData(5);

            this.fizzBuzzList[4].Should().BeOneOf("buzz", "wuzz");
        }

        /// <summary>
        /// GetFizzBuzzData method returns 'fizz buzz' when number is divisible by three and five.
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnFizzBuzz_WhenNumberIsDivisibleByThreeAndFive()
        {
            this.fizzRule.Setup(m => m.ModCheck(15)).Returns(true);
            this.fizzRule.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.buzzRule.Setup(m => m.ModCheck(15)).Returns(true);
            this.buzzRule.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzList = this.service.GetFizzBuzzData(15);

            this.fizzBuzzList[14].Should().BeOneOf("fizz buzz", "wizz wuzz");
        }

        /// <summary>
        /// GetFizzBuzzData method returns number when not divisible by three and five.
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnIntegerValue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            this.defaultRule.Setup(m => m.ModCheck(1)).Returns(true);
            this.defaultRule.Setup(m => m.Print(1, It.IsAny<DayOfWeek>())).Returns("1");

            this.fizzBuzzList = this.service.GetFizzBuzzData(1);

            this.fizzBuzzList[0].Should().Be("1");
        }
    }
}
