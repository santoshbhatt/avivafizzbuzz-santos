﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.WebController.Tests.Common
{
    public class TestModelHelper
    {
        /// <summary>
        /// To Validate Model object
        /// </summary>
        /// <param name="model"> Model object </param>
        /// <returns> Model Validate Results </returns>
        public static List<ValidationResult> Validate(object model)
        {
            var results = new List<ValidationResult>();
            var validationContext = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, validationContext, results, true);
            if (model is IValidatableObject)
            {
                (model as IValidatableObject).Validate(validationContext);
            }

            return results;
        }
    }
}
