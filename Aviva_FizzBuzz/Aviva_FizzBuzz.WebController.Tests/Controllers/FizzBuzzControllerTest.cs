﻿using System;
using System.Linq;
using NUnit.Framework;
using Moq;
using FluentAssertions;
using System.Web.Mvc;
using Aviva_FizzBuzz.Controllers;
using Aviva_FizzBuzz.ViewModels;
using Aviva_FizzBuzz.WebController.Tests.Common;
using Aviva_FizzBuzz.Service.Contract;
using System.Collections.Generic;

namespace Aviva_FizzBuzz.WebController.Tests.Controllers
{
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private FizzBuzzController controller;

        private FizzBuzzViewModel viewModel;

        private Mock<IFizzBuzzService> service;

        [SetUp]
        public void SetUpTests()
        {
            service = new Mock<IFizzBuzzService>();
            controller = new FizzBuzzController(this.service.Object);
            viewModel = new FizzBuzzViewModel();
        }

        /// <summary>
        /// Returns FizzBuzz view when FizzBuzzUserInput action is called.
        /// </summary>
        [Test]
        public void FizzBuzzUserInput_ShouldReturnFizzBuzzViewName_WhenCalled()
        {
            ViewResult result = controller.FizzBuzzUserInput() as ViewResult;

            result.ViewName.Should().Be("FizzBuzz");
        }

        /// <summary>
        /// Return validation error when user enter negative number
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnModelStateError_WhenUserInputIsNegative()
        {
            this.viewModel.UserInput = -1;
            string expectedErrorMessage = "Number must be positive";

            var result = TestModelHelper.Validate(this.viewModel);
            result.Any(r => r.ErrorMessage == expectedErrorMessage).Should().BeTrue();
        }

        /// <summary>
        /// Model state valid when user input positive number
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnValidModelState_WhenUserInputIsPositive()
        {
            this.viewModel.UserInput = 1;
            string expectedErrorMessage = string.Empty;

            var result = TestModelHelper.Validate(this.viewModel);
            result.Should().BeEmpty();
        }

        /// <summary>
        /// Returns validation error when user enter value out of defined range (1 to 1000).
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnModelStateError_WhenUserInputIsOutOfRange()
        {
            this.viewModel.UserInput = 1001;
            string expectedErrorMessage = "Please enter number between 1 to 1000";

            var result = TestModelHelper.Validate(this.viewModel);
            result[0].ToString().Should().Be(expectedErrorMessage);
        }

        /// <summary>
        /// Validating pagination logic
        /// </summary>
        [Test]
        public void GetFizzBuzzList_ShouldReturnModelCount_PaginationSize()
        {
            this.viewModel.UserInput = 21;
            this.service.Setup(m => m.GetFizzBuzzData(It.IsAny<int>())).Returns(this.ReturnsMockFizzBuzzData(DayOfWeek.Monday));
            var obj = new FizzBuzzController(this.service.Object);

            ViewResult actionResult = (ViewResult)obj.GetFizzBuzzList(this.viewModel);
            var model = actionResult.Model as FizzBuzzViewModel;

            model.DisplayData.Count.Should().Be(20);
        }

        /// <summary>
        /// Validating pagination
        /// </summary>
        /// <param name="dayOfWeek"> Input day of the week </param>
        /// <returns>Mocked fizzbuzz list output </returns>
        public List<string> ReturnsMockFizzBuzzData(DayOfWeek dayOfWeek)
        {
            string fizz = dayOfWeek == DayOfWeek.Wednesday ? "wizz" : "fizz";
            string buzz = dayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz";

            List<string> objLstDisplayMessage = new List<string>();
            objLstDisplayMessage.Add("1");
            objLstDisplayMessage.Add("2");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("4");
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("7");
            objLstDisplayMessage.Add("8");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add("11");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("13");
            objLstDisplayMessage.Add("14");
            objLstDisplayMessage.Add(fizz + " " + buzz);
            objLstDisplayMessage.Add("16");
            objLstDisplayMessage.Add("17");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("19");
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add(fizz);
            return objLstDisplayMessage;
        }
    }
}
