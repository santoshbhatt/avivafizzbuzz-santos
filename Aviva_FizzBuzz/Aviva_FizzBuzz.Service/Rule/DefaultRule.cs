﻿using Aviva_FizzBuzz.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Rule
{
    public class DefaultRule : IRule
    {
        /// <summary>
        /// Modolous operation check
        /// </summary>
        /// <param name="value"> Integer input </param>
        /// <returns>True when value is not divisible by three and five </returns>
        public virtual bool ModCheck(int value)
        {
            return value % 3 != 0 && value % 5 != 0;
        }

        /// <summary>
        /// Print value
        /// </summary>
        /// <param name="value"> Input integer </param>
        /// <param name="dayOfWeek"> Passed day of week </param>
        /// <returns> String based on condition </returns>
        public virtual string Print(int value, DayOfWeek dayOfWeek)
        {
            return value.ToString();
        }
    }
}
