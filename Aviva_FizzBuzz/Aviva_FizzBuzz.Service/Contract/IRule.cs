﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Contract
{
    public interface IRule
    {
                /// <summary>
        /// Modolous operation check
        /// </summary>
        /// <param name="value"> Integer value </param>
        /// <returns>True when condition matches</returns>
        bool ModCheck(int value);

        /// <summary>
        /// Print value
        /// </summary>
        /// <param name="value"> Integer value </param>
        /// <param name="dayOfWeek"> Passed day of week </param>
        /// <returns> String based on condition </returns>
        string Print(int value, DayOfWeek dayOfWeek);
    }
}
