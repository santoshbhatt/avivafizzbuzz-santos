﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Contract
{
    public interface IFizzBuzzService
    {
        /// <summary>
        /// Output FizzBuzz data
        /// </summary>
        /// <param name="value"> Integer value </param>
        /// <returns> FizzBuzz list </returns>
        List<string> GetFizzBuzzData(int value);
    }
}
