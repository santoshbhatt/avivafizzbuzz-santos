﻿using Aviva_FizzBuzz.Service.Contract;
using Aviva_FizzBuzz.Service.Rule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aviva_FizzBuzz.Service.Service
{
    public class FizzBuzzService : IFizzBuzzService
    {
        private List<IRule> rules;

        public FizzBuzzService(List<IRule> rules)
        {
            this.rules = rules;
        }

        /// <summary>
        /// Output FizzBuzz data
        /// </summary>
        /// <param name="value"> Integer value </param>
        /// <returns> FizzBuzz list </returns>
        public List<string> GetFizzBuzzData(int values)
        {
            List<string> fizzBuzzList = new List<string>();
            string fizzBuzzData = null;

            for (int value = 1; value <= values; value++)
            {
                fizzBuzzData = string.Join(" ", this.rules.Where(r => r.ModCheck(value)).Select(r => r.Print(value, DateTime.Now.DayOfWeek)));
                fizzBuzzList.Add(fizzBuzzData);
            }

            return fizzBuzzList;
        }
    }
}
